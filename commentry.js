/*
** Commentry library
*/

const COMMENTRY_FORM_TEMPLATE = `
  <p><input placeholder='Your name or nick, or leave blank for Anonymous' type='text' id='name' name='name'/><br/>
  <textarea placeholder='Enter your comment here' id='comment' name='comment'/></textarea></p>
  <p><input id='submit_btn' type='submit' value='Submit'/></p>
`;
const COMMENTRY_MODERATION = `<strong>Hi there!</strong>
  <p>Please note that moderation is enabled and your comment is pending review.
  Please do not resubmit.</p>`;
const COMMENTRY_MISMATCH = `<strong>User/password mismatch</strong>
  <p>This might be because you are trying to use a username that is already
  taken, or use one you've used from another device or browser.  Usernames
  cannot be reused with this system.  Currently working on this functionality.
  </p>`;
const COMMENTRY_UNKNOWN_ERROR = `<strong>Something went wrong with your
  submission.</strong><p>Sorry, I do not have a record.  Maybe try again
  later?  The error was: "# ERROR #".</p>`;
const COMMENTRY_SERVER_ISSUE = `Could not load comments.  There appears
  to be a server issue.  Disabling, sorry.`;
const COMMENTRY_NO_COMMENTS = `<p title='(Yet.)'>There are no comments on this.</p>`;

function generate_token(len) {
  ar = ["A","B","C",'D','E','F','G','H','I','J','K','L','M','N','O','P','Q',
        "R","S","T",'U','V','W','X','Y','Z','a','b','c','d','e','f','g','h',
        "i","j","k",'l','m','n','o','p','q','r','s','t','u','v','w','x','y',
        "z","0","1",'2','3','4','5','6','7','8','9','_','-'];
  ba = new Uint8Array(len || 10);
  window.crypto.getRandomValues(ba);
  return(Array.from(ba, x => ar[x % 64]).join(''));
}

class Commentry {
  
  constructor(url, config) {

    // TODO: fail if undefined
    this.serverUrl = url;

    // document element IDs
    this.containerDivId = 'commentry';
    this.commentsDivId = 'comments';
    this.commentsFormId = 'commentform';

    // initialize user information (if available)
    this.username = getCookie('user');
    if (this.username) {
      this.usertoken = getCookie('token');
      if (!this.usertoken) {
        this.usertoken = generate_token(16);
        setCookie('token', this.usertoken);
      }
    }

    // set up templates
    this.formTemplate = COMMENTRY_FORM_TEMPLATE;
    this.moderationMessage = COMMENTRY_MODERATION;
    this.mismatchMessage = COMMENTRY_MISMATCH;
    this.unknownError = COMMENTRY_UNKNOWN_ERROR;
    this.serverIssue = COMMENTRY_SERVER_ISSUE;
    this.noComments = COMMENTRY_NO_COMMENTS;
    if (config) {
      if (config['formTemplate']) { this.formTemplate = config['formTemplate'] }
      if (config['moderationMessage']) { this.moderationMessage = config['moderationMessage'] }
      if (config['mismatchMessage']) { this.mismatchMessage = config['mismatchMessage'] }
      if (config['unknownError']) { this.unknownError = config['unknownError'] }
      if (config['serverIssue']) { this.serverIssue = config['serverIssue'] }
      if (config['noComments']) { this.noComments = config['noComments'] }
    }
  }

  renderForm() {
    document.getElementById(this.containerDivId).innerHTML = 
      `<div id='${this.commentsDivId}'>
       </div>
       <form class='comment' id='${this.commentsFormId}'>
        <input type='hidden' id='token' name='token' value=''/>
        <input type='hidden' id='article' name='article' value=''/>`
      + this.formTemplate
      + `</form>`;
  }

  submitForm() {
    var form = document.getElementById(this.commentsFormId);

    // store username in cookie if necessary
    if (!this.username && form.name.value !== "") {
      this.username = form.name.value;
      setCookie('user', this.username);

      // generate and store token if necessary
      this.usertoken = generate_token(16);
      setCookie('token', this.usertoken);
      form.token.value = this.usertoken;
    }

    // create request for form submission
    var xhr = new XMLHttpRequest();

    // bind formdata object and form element
    const fd = new FormData(form);

    commentry = this;
    xhr.onreadystatechange = function(event) {
      if (this.readyState == 4) {
        if (this.status == 201) {
          refresh_comments();
          form.reset();
        } else if (this.status == 202) {
          // moderated
          commentry.commentFlash(commentry.moderationMessage);
          form.reset();
        } else if (this.status == 403) {
          // this is a username/password mismatch.  This might be because
          // the user is trying to use a username already in use by somebody
          // else or one they have already used in another browser.
          // TODO: Provide a link to the dashboard for if they want to change
          // their password in order to use something they know.
          commentry.commentFlash(commentry.mismatchMessage);
        } else {
          // error
          var errorMessage = commentry.unknownError.replace('# ERROR #', event.target.responseText);
          commentry.commentFlash(errorMessage);
        }
      }
    };

    // set up request
    xhr.open("POST", this.serverUrl + "/api/comments?article=" + window.location.pathname, true);

    // send data
    xhr.send(fd);
  }

  activateForm(article_path) {
    // get comment form
    const form = document.getElementById(this.commentsFormId);

    // display
    form.style.display = 'initial';

    // populate form elements about article
    form.article.value = article_path;

    // populate user information
    this.populateUser();

    // add listener to comment form submit button
    commentry = this;
    form.addEventListener("submit", function(event) {
      event.preventDefault();
      commentry.submitForm();
    });

    // add listener for form resets to repopulate user info
    form.addEventListener("reset", function(event) {
      commentry.populateUser();
    });

    // if user defined in session, forbid editing
    if (this.username) {
      // disable form field
      form.name.readOnly = true;
    }
  }

  populateUser() {
    if (!this.username) {
      return;
    }
    const form = document.getElementById(this.commentsFormId);
    form.name.value = this.username;
    form.token.value = this.usertoken;
  }

  showComments(comments) {
    var commentsDiv = document.getElementById(this.commentsDivId);
    commentsDiv.innerHTML = "";

    if (comments == "") {
      commentsDiv.insertAdjacentHTML("afterbegin", this.noComments);
      return;
    }

    var comments_arr = JSON.parse(comments);

    // create list element
    var list_el = document.createElement("DL");
    commentsDiv.appendChild(list_el);

    // populate list of comments
    var comment, title_el, timestamp, title_text, comment_el;
    for (var comment of comments_arr) {
      title_el = document.createElement("DT");
      timestamp = new Date(comment["submitted"]);
      title_text = 
        comment["submitter"] 
        + " :: " 
        + timestamp.toLocaleDateString() 
        + ", " 
        + timestamp.toLocaleTimeString();
      title_el.appendChild(document.createTextNode(title_text));

      comment_el = document.createElement("DD");
      comment_el.insertAdjacentHTML('beforeend', comment["comment"]);

      list_el.appendChild(title_el);
      list_el.appendChild(comment_el);
    }
  }

  noComments(xhr) {
    var commentsDiv = document.getElementById(this.commentsDivId);
    commentsDiv.innerHTML = "";

    var para = document.createElement("P");
    para.textContent = this.serverIssue;
    para.title = "Status " + xhr.status + ", response text '" + xhr.responseText + "'";
    commentsDiv.appendChild(para);

    // disable comment form
    var commentsForm = document.getElementById(this.commentsFormId);
    commentsForm.style.display = "none";
  }

  refreshComments() {
    var xhttp = new XMLHttpRequest();
    commentry = this;
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4) {
        if (this.status == 200) {
          commentry.showComments(this.responseText);
        } else if (this.status == 404) {
          // TODO: hack
          commentry.showComments(this.responseText);
        } else {
          commentry.noComments(this);
        }
      }
    };

    xhttp.open("GET", this.serverUrl + "/api/comments?article=" + window.location.pathname, true);
    xhttp.send();
  }

  commentFlash(html) {

    // create flash div
    var flash = document.createElement("DIV");
    flash.classList.add('flash');
    flash.innerHTML = html;

    // append to end of comments, right above form
    var commentsDiv = document.getElementById(this.commentsDivId);
    commentsDiv.appendChild(flash);

    // temporarily disable comment form
    var commentsForm = document.getElementById("commentform");
    commentsForm.style.display = "none";
  }

}

// on window initialization
window.addEventListener("load", function() {

  commentry.renderForm();
  commentry.activateForm(window.location.pathname);
  commentry.refreshComments();

});
