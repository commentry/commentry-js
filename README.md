# commentry-js

Javascript client for Commentry, a minimal commentary system.

## Usage

1. Include in your site's JavaScript.  All files must be included explicitly.
    ```html
      <!-- Commentry -->
      <script type='text/javascript' src='js/commentry/cookies.js'></script>
      <script type='text/javascript' src='js/commentry/commentry.js'></script>
    ```
2. Somewhere in your page or template, instantiate Commentry with the address
of the server:
    ```html
    <script>
      // fire up Commentry
      commentry = new Commentry('http://127.0.0.1:5000');
    </script>
    ```
3. Where you want the comments and entry form to go, place an appropriate
`<div>`:
    ```html
    <h2>Comments</h2>
    <div id='commentry'>
      <noscript>JavaScript must be enabled for comments.</noscript>
    </div>
    ```

### Stylesheets

The following style elements are available:

* `commentry`: the container for all Commentry stuff.
* `comments`: container for comments.
* `commentform`: the form for comments.

Elements within these containers depend partially on configuration.
