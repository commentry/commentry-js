/*
** Cookies library
**
** This needs to be included from code using Commentry.
*/
// --------------------------------------------------------------------------
//                                                           cookie functions
// --------------------------------------------------------------------------

var cookies = { "__initialized" : false };

function makeCookieMap() {
  var arr = decodeURIComponent(document.cookie).split(';');

  // iterate through name=value pairs
  for (str of arr) {
    while (str.charAt(0) == ' ') {
      str = str.substring(1);
    }
    pair = str.split('=');
    cookies[pair[0]] = pair[1];
  }

  cookies["__initialized"] = true;
}

function getCookie(name) {
  if (!cookies["__initialized"]) {
    makeCookieMap();
  }
  return cookies[name];
}

function setCookie(name, value, daystolive=365) {
  // determine expiry
  var d = new Date();
  d.setTime(d.getTime() + (daystolive * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();

  // set cookie
  document.cookie = name + "=" + value + ";" + expires + ";path=/";

  // add to cookie map
  cookies[name] = value;
}

